# Introduction:
TwigEasyDebug streamlines the Drupal development process by allowing users to easily enable Twig debugging and verbose configuration settings directly from the UI. Say goodbye to the hassles of manually editing service files and embrace a simplified debugging experience!

# Features
- **User-Friendly UI radio button:** No more delving into configuration files. Use our straightforward interface to manage your debugging settings.
- **Two-in-One Functionality:** Handle both Twig debugging and verbose configurations in one place.
- **Safe Implementation:** The module creates a copy of your services file, ensuring the original remains untouched and secure.

# Post-Installation
Upon installation, navigate to the configuration page of `TwigEasyDebug` in the Drupal admin panel. You'll be presented with radio buttons to manage your debugging preferences, and upon saving, the services copy will be updated accordingly. Or you can also access it from web/admin/debug/form

Additional Requirements
`TwigEasyDebug` operates solely with Drupal core. No external dependencies required.

# Recommended modules/libraries
No additional modules or libraries are recommended at this time.

Similar projects
While there might be modules offering debugging tools, `TwigEasyDebug` is unique in its focus on providing a hassle-free, UI-driven experience specifically for Twig debugging and verbose configurations.

# MAINTAINERS

- https://www.drupal.org/u/bhupendra_raykhere
- https://www.drupal.org/u/priyansh-chourasiya
- https://www.drupal.org/u/vijaysharma_89