<?php
namespace Drupal\twig_easy_debug\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Implements a configuration form.
 */
class EnableTwigSuggestion extends ConfigFormBase {
      /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;
   /**
   * {@inheritdoc}
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system')
    );
  }
/**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return[
      'twig_easy_debug.settings',
    ];
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twig_easy_debug_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
     $config = $this->config("twig_easy_debug.settings");
    // Add a checkbox for file duplication.
       // Add a checkbox for file duplication.
       $form['duplicate_file'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create Services.yml file'),
        '#default_value' => $config->get('duplicate_file'),
      ];
      $form['verbose'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Verbose for Debug'),
        '#default_value' => $config->get('verbose'),
      ];
      // Add other form elements as needed.
      return parent::buildForm($form, $form_state);
    }
  
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      // Check if the checkbox is selected.
      $duplicate_file = $form_state->getValue('duplicate_file');
      $verbose = $form_state->getValue('verbose');
      parent::submitForm($form, $form_state);
      $config = $this->config('twig_easy_debug.settings')
         ->set('duplicate_file',$duplicate_file)
         ->set('verbose',$verbose)
         ->save();
      $duplicate_file = $config->get('duplicate_file');
  
      if ($duplicate_file) {
        // Copy the default services.yml to a new file.
        $source_file = DRUPAL_ROOT . '/sites/default/default.services.yml';
        $destination_file = DRUPAL_ROOT . '/sites/default/services.yml';
  
        if (file_exists($source_file)) {
          $file_content = file_get_contents($source_file);
  
          // Update debug to true and cache to false.
          $file_content = str_replace('debug: false', 'debug: true', $file_content);
          $file_content = str_replace('cache: true', 'cache: false', $file_content);
  
          // Save the modified content to the new file.
          file_put_contents($destination_file, $file_content);
        }
      }else {
        // Delete the file if the checkbox is unchecked.
        $destination_file = DRUPAL_ROOT . '/sites/default/services.yml';
        if (file_exists($destination_file)) {
          $this->fileSystem->unlink($destination_file);
        }
      }
  // Check if the second checkbox is selected.
   $modify_settings = $config->get('verbose');

  if ($modify_settings) {
    // Modify settings.php to add the configuration.
    $settings_file = DRUPAL_ROOT . '/sites/default/settings.php';

    if (file_exists($settings_file)) {
      $settings_content = file_get_contents($settings_file);
      // Find the position of the configuration line.
      $start = strpos($settings_content, "\$config['system.logging']['error_level'] = 'verbose';");
      if ($start == false) {
      // Add the configuration to settings.php.
      $settings_content .= "\n";
      $settings_content .= '$config[\'system.logging\'][\'error_level\'] = \'verbose\';';

      // Save the modified content back to settings.php.
      file_put_contents($settings_file, $settings_content);
    }
    }
   } else {
    
    // Remove the configuration if the checkbox is unchecked.
   // Remove the configuration if the checkbox is unchecked.
$settings_file = DRUPAL_ROOT . '/sites/default/settings.php';

if (file_exists($settings_file)) {
  $settings_content = file_get_contents($settings_file);

  // Find the position of the configuration line.
  $start = strpos($settings_content, "\$config['system.logging']['error_level'] = 'verbose';");

  if ($start !== false) {
    // Calculate the length of the configuration line.
    $length = strlen("\$config['system.logging']['error_level'] = 'verbose';");
    // Remove the configuration line from the content.
    $settings_content = substr_replace($settings_content, '', $start, $length);
    // Remove multiple consecutive blank lines.
    $settings_content = preg_replace("/\n\s*\n/", "\n", $settings_content);

    // Save the modified content back to settings.php.
    file_put_contents($settings_file, $settings_content);
  }
}

  }
    }
  
  }


